#!/bin/zsh
if ! timeout 1s xset q &>/dev/null; then
    [[ "$(tty)" = "/dev/tty1" ]]; pgrep qtile || startx "$XDG_CONFIG_HOME/X11/xinitrc"
fi
