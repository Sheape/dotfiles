-- +++ NEOVIM CONFIG  +++ --
--
-- Maintainer: Sheape
-- Email: sheape@tutanota.com
-- Website: https://gitlab.com/Sheape/dotfiles
-- Initially created: 01-05-22

require("keymaps")
require("workman")
require("colorscheme")
require("plugins")
require("settings/general")
